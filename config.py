import discord
import json
import sys


class Config:
    def __init__(self, logger):
        """Set defauld config values"""
        self.logger = logger.getChild("Config")

        self.retry_count = 3
        self.separator = "------------------------------\n"
        self.embed = True
        self.conf_file = "sheriff.json"
        self.token_file = "sheriff_token.key"
        # Test version
        #self.conf_file = "sheriff_test.json"
        #self.token_file = "./sheriff_token_test.key"
        self.bot_prefix = ("?")

        self.check_user_interval = 30*60

        self.role_admin_name="Admin"
        self.role_newcomer_name="Newcomer"
        self.role_member_name="Member"
        self.role_muted_name="Muted"

        self.role_admin = discord.role
        self.role_newcomer = discord.role
        self.role_member = discord.role
        self.role_muted = discord.role

        self.promote_to_newcomer_hours = 1
        self.promote_to_member_days = 14
        self.promote_to_member_message_count = 10
        
        self.punish_action="mute"
        self.punish_vote_count=5
        self.punish_emoji=":radioactive:"

        self.db_filename="sheriff.db"

        self.prune_days = 60

        self.server_name = ""
        self.server_id = ""


        self.client_id = ""
        self.token = ""

        self.bot_channel_id = ""
        self.bot_channel = discord.channel

        self.server = discord.guild

        self.welcome_info = "Welcome to ArmaOnUnix!"


    def load_conf(self):
        """Loads bot configuration from CONF_FILE."""
        try:
            with open(self.conf_file) as file:
                data = json.load(file)
        except IOError:
            self.logger.getChild("load_conf").error(f"Opening {self.conf_file} failed!")
            sys.exit(1)
        except json.JSONDecodeError as e:
            self.logger.getChild("load_conf").error(f"JSON Decoding {self.conf_file} failed!\n{e}")
            sys.exit(1)

        self.role_admin_name = data.setdefault("role_admin", self.role_admin)
        self.role_newcomer_name = data.setdefault("role_newcomer", self.role_newcomer)
        self.role_member_name = data.setdefault("role_member", self.role_member)
        self.role_muted_name = data.setdefault("role_muted", self.role_muted)

        self.promote_to_newcomer_hours = data.setdefault("promote_to_newcomer_hours", 
                                                         self.promote_to_newcomer_hours)
        self.promote_to_member_days = data.setdefault("promote_to_member_days", 
                                                      self.promote_to_member_days)
        self.promote_to_member_message_count = data.setdefault("promote_to_member_message_count",
                                                               self.promote_to_member_message_count)

        self.punish_action = data.setdefault("punish_action", self.punish_action)
        self.punish_vote_count = data.setdefault("punish_vote_count", self.punish_vote_count)
        self.punish_emoji = data.setdefault("punish_emoji", self.punish_emoji)

        self.db_filename = data.setdefault("db_filename", self.db_filename)

        self.prune_days = data.setdefault("prune_days", self.prune_days)

        self.server_name = data.setdefault("server_name", self.server_name)
        self.server_id = data.setdefault("server_id", self.server_id)

        self.bot_channel_id = data.setdefault("bot_channel_id", self.bot_channel_id)

    def load_welcome_info(self):
        try:
            with open("info.md", "r") as file:
                self.welcome_info = file.read()
        except Exception as e:
            self.logger.getChild("load_welcome_info").error(f"Loading welcome info failed!\n{e}")

    def load_tokens(self):
        """Loads bot credentials from TOKEN_FILE."""
        try:
            file = open(self.token_file)
            data = json.load(file)
        except IOError:
            self.logger.getChild("load_tokens").error(f"Opening failed! {self.token_file}")
            sys.exit(1)
        except json.JSONDecodeError as e:
            self.logger.getChild("load_tokens").error(f"JSON Decoding {self.token_file} failed!\n{e}")
            sys.exit(1)

        if "client_id" not in data:
            self.logger.getChild("load_tokens").error("Missing bot client_id !")
            sys.exit(1)
        else:
            self.client_id = data["client_id"]
        if "secret" not in data:
            self.logger.getChild("load_tokens").error("Missing bot secret !")
            sys.exit(1)
        else:
            self.token = data["secret"]

        self.logger.getChild("load_tokens").info("Token loaded succesfully!")
