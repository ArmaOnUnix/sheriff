#!/usr/bin/env bash
# you need to manually copy/create in the destination dir token.key, server.conf and links.conf files
docker stop $(docker ps -a | grep sheriff | cut -d' ' -f1)
docker rm $(docker ps -a | grep sheriff | cut -d' ' -f1)
docker rmi sheriff
docker build -t sheriff .
