# Sheriff bot
Discord bot that serves for fighting spammers. It works with 4 roles: **Admin**, **Member**, **Newcomer** and **Muted**. 
Everyone who joins the discord server has no role except @everyone. After an hour (by default) the user gets **Newcomer** 
role. After logging into the discord channel for 14 days (does not have to be in row) and writing at least 10 messages 
he/she becomes **Member**. Members typically have higher rights such as attaching images, posting links. Members also have 
the right to vote to punish a spammer or toxic user using certain reaction on his/her comments. This reaction
is set in the `sheriff.json` file (punish_emoji), by default its set to ☢ (:radioactive:). At the current stage you need to
use the unicode code (https://unicode.org/emoji/charts/full-emoji-list.html) as it does not work with the string 
interpretation of the emoji nor the emoji symbols.


## Roles:
**No role**: All new users that come here have no role and are not able to write messages for one hour
after they joined our Discord. This limitation has been applied in order to stop spamming.  
**Newcomer**: These users have been on our Discord server for at least an hour and now are allowed
to write messages and speak on Voice channels. These users however cannot post links nor attach files 
and also can not voteban/votekick/votemute.  
**Members**: These users have been on our Discord for at least 14 days and have written at least
10 messages. These users have all the rights as Newcomers, but they can also post links, attach files
and voteban/votekick/votemute.  
**Admin**: Super users with highest rights.


## How it works
Sheriff bot is on 24/7, works on all channels and logs into SQLite database the login times of all 
server members. It works with this data in order to determine if a newcomer is eligible to become 
member or not. Changes users role to **muted** if he/she got five punish emojis on his/her comment.
Users with admin role cannot be banned by the bot. 

## How to use
1. Clone this repository to the directory where you want to store your bot files. 
E.g.:
    ```bash
    cd ~
    git clone https://gitlab.com/ArmaOnUnix/sheriff.git
    cd sheriff
    ```
2. Change the `sheriff.conf` to fit your preferences. You can find the available settings in the 
**Configuration** section.
3. Create a file called **sheriff_token.key**. Copy the data from the sheriff_token_example or from
the **Configuration** section below (remove the comments). Change the bot_id and the secret values
to your values.  If you want to use the bot in Docker, follow this step, otherwise go to the 5th 
step. 
4. To build the docker image run the command `bash build_sheriff.sh`. This will remove all old 
versions of the sheriff docker image and build a new one from the newest version.
5. To run the bot (without docker) just use the command `python3 sheriff.py`. If you are using 
docker, run the command `bash run_sheriff.sh`
 
## Configuration
**sheriff.conf**
```json
{
    // name of the admin role on your Discord server
    "role_admin": "Admin",
    // name of the newcomer role on your Discord server
    "role_newcomer": "Newcomer",
    // name of the member role on your Discord server
    "role_member": "Member",
    // name of the muted role on your Discord server
    "role_muted": "Muted",

    // number of hours after which a new user becomes Newcomer
    "promote_to_newcomer_hours": 0.05,
    // number of hours after which a user becomes Member
    "promote_to_member_days": 1,
    // number of messages a user needs to post in order to become Meber
    "promote_to_member_message_count": 3,

    // supported punish actions: "mute", "kick", "ban"
    "punish_action": "mute",
    // number of punish reactions on comment to execute ban action on user
    "punish_vote_count": 1,
    // emoji used to vote punish
    "punish_emoji": "\u2622",
    // name of the database file used
    "db_filename": "sheriff.db",
    // day limit for removing older users from the server
    "prune_days": 60,
    // name of the Discord server
    "server_name": "test_server",
    // id of the Discord server
    "server_id": "387370249323937802",
    // bot channel
    "bot_channel": "473186979031285771"
}
```
**sheriff_token.key**
```json
{
    // bot id
    "client_id": "595380168260884516",
    // bot secret - this secret token is needed in order for the bot to work
    // if you dont know how to obtain it I reccomend watching the beggining of this video: 
    // https://www.youtube.com/watch?v=ZNA7Eij3UmY
    "secret": "NTI1Mn3iMTY2MjYwODgzNTE2-Dv-oHw.LBQNlnN78hFAVlAkXirOaVyfSx4"
}
```

## Commands
**?restart** - Restarts the Sheriff bot.  
**?shutdown**- Turns off the Sheriff bot.  
**?help**    - Shows this message.  
**?stats**   - Shows statistic info - Not yet implemented.  
**?update**  - Updates bot from the repository and restarts the bot.