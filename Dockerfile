FROM python:3.7

RUN apt-get update; apt-get -yqq upgrade
RUN apt-get install -yqq python3-pip git
#RUN cd /; git clone https://gitlab.com/ArmaOnUnix/Armanator.git

RUN mkdir /sheriff
RUN useradd -m sheriff
RUN chown -R sheriff /sheriff
USER sheriff
WORKDIR /sheriff

RUN pip install --upgrade pip
COPY requirements.txt .
RUN pip install --user -r requirements.txt && rm requirements.txt

VOLUME /sheriff

STOPSIGNAL SIGINT
CMD ["python3", "sheriff.py"]

