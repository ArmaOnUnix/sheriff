# This file contains database which stores mission-related server data
from datetime import datetime, timedelta
import sys
from sqlalchemy import Column, ForeignKey, Integer, String, Float, Boolean, engine, desc, func
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.exc import OperationalError

Base = declarative_base()


class Users(Base):
    __tablename__ = 'users'
    user_id = Column(Integer, primary_key=True)
    user_name = Column(String, nullable=False)
    message_count = Column(Integer)
    days_count = Column(Integer)
    last_login = Column(String)
    muted = Column(Boolean)


# CHECK
class Database:
    def __init__(self, config, logger):
        self.logger = logger.getChild("Database")
        self.config = config
        self.load_db()

    def load_db(self):
        try:
            file_name = 'sqlite:///' + self.config.db_filename
            self.engine = create_engine(file_name)
            self.create_tables()
            # Bind the engine to the metadata of the Base class so that the
            # declaratives can be accessed through a DBSession instance
            Base.metadata.bind = self.engine
            DBSession = sessionmaker(bind=self.engine)
            # DBSession.configure(bind=engine)
            # A DBSession() instance establishes all conversations with the database and represents
            # a "staging zone" for all the objects loaded into the database session object.
            self.session = DBSession()
        except Exception as e:
            self.logger.getChild("load_db").error(f"{e}.")
            sys.exit(1)

    def close_db(self):
        self.logger.getChild("close_db").info("Closing the database...")
        self.session.close()

    def create_tables(self):
        try:
            Base.metadata.create_all(self.engine)
        except Exception as e:
            self.logger.getChild("create_tables").error(f"{e}")

    def trim(self, s: str):
        s = s.replace("\'", "")
        s = s.replace('\"', '')
        return s

    # CHECK test
    def add_new_user(self, user_id, user_name, last_login, muted=False):
        try:
            temp_user = self.session.query(Users).filter(Users.user_id == user_id).first()
            # check if user already exists
            if temp_user is not None:
                return
            else:
                self.logger.getChild("add_new_user").debug(f"Adding new user {user_name}")

                # DELETE
                # flag = 0
                # if muted:
                #     flag = 1

                # TODO CHECK if muted is saved as int in DB
                last_login_str = last_login.strftime("%y-%m-%dT%H:%M:%S")
                new_user = Users(user_id=user_id, user_name=user_name, message_count=0,
                                 days_count=0, last_login=last_login, muted=muted)
                self.session.add(new_user)
                self.session.commit()
        except Exception as e:
            self.logger.getChild("add_new_user").error(f"{e}")

    # CHECK test
    def insert_login_time(self, user_id, last_login: datetime, midnight=False):
        """Inserts login time."""
        try:
            self.logger.getChild("insert_login_time").debug(f"Adding new login time for {user_id}")
            temp_user = self.session.query(Users).filter(Users.user_id == user_id).first()
            old_login = datetime.strptime(temp_user.last_login, "%y-%m-%dT%H:%M:%S")

            # if the new login date is higher than the old one increment days, otherwise ignore
            if old_login.date() < last_login.date():
                result = self.increment_days_count(user_id)
                # if its not an midnight update, update the last_login timestamp
                if not midnight:
                    last_login_str = last_login.strftime("%y-%m-%dT%H:%M:%S")
                    temp_user.last_login = last_login_str
                    self.session.commit()
                return result
        except Exception as e:
            self.logger.getChild("insert_login_time").error(f"{e}")
        return False

    # CHECK test
    def is_muted(self, user_id):
        try:
            temp_user = self.session.query(Users).filter(Users.user_id == user_id).first()
            if temp_user is not None:
                # TODO CHECK dunno if it returns bool or int
                # if temp_user.muted != 0:
                #     return True
                # else:
                #     return False
                return temp_user.muted
        except Exception as e:
            self.logger.getChild("is_muted").error(f"{e}")

    # CHECK test
    def set_muted(self, user_id, muted=True):
        try:
            # flag = 0
            # if muted:
            #     flag = 1
            temp_user = self.session.query(Users).filter(Users.user_id == user_id).first()
            # temp_user.muted = flag
            temp_user.muted = muted
            self.session.commit()
        except Exception as e:
            self.logger.getChild("set_muted").error(f"{e}")

    # CHECK test
    def increment_days_count(self, user_id):
        """Increments message count for every new message added."""
        try:
            self.logger.getChild("increment_days_count").debug(f"Incrementing days count for "
                                                               f"{user_id}")
            temp_user = self.session.query(Users).filter(Users.user_id == user_id).first()
            temp_user.days_count += 1
            self.session.commit()
            return self.check_promotion(user_id)
        except Exception as e:
            self.logger.getChild("increment_days_count").error(f"{e}")
        return False

    # CHECK test
    def increment_message_count(self, user_id):
        """Increments message count for every new message added."""
        try:
            self.logger.getChild("increment_message_count").debug(f"Incrementing message count for "
                                                                  f"{user_id}")
            temp_user = self.session.query(Users).filter(Users.user_id == user_id).first()
            temp_user.message_count += 1
            self.session.commit()
            return self.check_promotion(user_id)
        except Exception as e:
            self.logger.getChild("increment_message_count").error(f"{e}")
        return False

    # CHECK test
    def decrement_message_count(self, user_id: int):
        """Decrements message count for every deleted message added."""
        # message_count has to be higher than 0, so that the user cannot have negative number
        # of messages - e.g. when deleting older messages, before the bot logged in
        try:
            self.logger.getChild("decrement_message_count").debug(f"Incrementing message count for "
                                                                  f"{user_id}")
            temp_user = self.session.query(Users).filter(Users.user_id == user_id).first()
            # query = f"""UPDATE users SET message_count = message_count + 1
            #             WHERE user_id={user_id};"""
            if temp_user.message_count > 0:
                temp_user.message_count -= 1
                self.session.commit()
        except Exception as e:
            self.logger.getChild("decrement_message_count").error(f"{e}")

    # CHECK test
    def prune_users(self):
        try:
            prune_ts = (datetime.utcnow() - timedelta(days=self.config.prune_days)).strftime(
                "%y-%m-%dT%H:%M:%S")
            self.session.query(Users).filter(Users.last_login < prune_ts).delete()
            self.session.commit()
        except Exception as e:
            self.logger.getChild("prune_users").error(f"{e}")

    # CHECK test
    def check_promotion(self, user_id):
        """Checks user status to find out if the user is eligible for promotion.
        Returns True if is and False if is not."""
        temp_user = self.session.query(Users).filter(Users.user_id == user_id).first()
        try:
            if temp_user is None:
                return False
            if temp_user.message_count >= self.config.promote_to_member_message_count and \
                    temp_user.days_count >= self.config.promote_to_member_days:
                return True
        except Exception as e:
            self.logger.getChild("check_promotion").error(f"{e}")
        return False

    # CHECK test
    def get_stats(self):
        try:
            data_msgs = self.session.query(func.sum(Users.message_count)).first()
            data_muted = self.session.query(func.count(Users.user_id)).filter(
                Users.muted == 1).first()
            return data_msgs[0], data_muted[0]
        except Exception as e:
            self.logger.getChild("get_stats").error(f"{e}")

    # CHECK test
    def get_stats_user(self, user_id):
        try:
            result = self.session.query(Users).filter(Users.user_id == user_id).first()
            return result
        except Exception as e:
            self.logger.getChild("get_stats_user").error(f"{e}")
