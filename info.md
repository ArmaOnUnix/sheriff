Welcome to ArmaOnUnix community! This is an open community for Arma 3  players on GNU/Linux and MacOS. We have our own server called emp's Missions server (armaonunix.duckdns.org:2303).

**We also have a few rules:**
1. Respect the admins!
2. Respect each other, we want to have here a friendly community not flame wars.
3. Do not spam and do not post malicious links or links to malicious content.
4. Have fun!

**Roles:**
We have here 4 roles: **Admin, Member, Newcomer, everyone**.
First when you join the server your only role is **everyone** - you cannot write messages nor use the voice channels.
After an hour you will be promoted to **Newcomer** and you will be able to write messages and use voice chat.  
After being online on this server for 14 days (does not have to be in row) and after you post at least 10 messages, 
you will be promoted to **Member**. As **Member** you will be able to post links, attach files and also votepunish 
spammers and toxic users using the :radioactive:  reaction. If a user has 5 or more :radioactive:  reactions on one 
comment, he/she will be muted.  

**Below you can find important community links:**  
**Web page:** https://armaonunix.gitlab.io/  
**Discord:** https://discord.gg/p28Ra36  
**Steam group:** https://steamcommunity.com/groups/ArmaOnUnix  
**Reddit:** https://www.reddit.com/r/armaonunix/  
**Email:** armaonunix@gmail.com  
**Gitlab:** https://gitlab.com/groups/ArmaOnUnix  