#!/usr/bin/env python3
import asyncio
import datetime
import json
import logging
from logging.handlers import RotatingFileHandler
import os
import sqlite3
import sys

from discord.ext.commands import Bot
import discord

from config import Config
from database import Database, Users


def init_logger():
    logger = logging.getLogger("discord")
    logger.setLevel(logging.INFO)
    handler = RotatingFileHandler("sheriff.log", maxBytes=5 * 1024 * 1024, backupCount=4,
                                  encoding="utf-8", mode="w")
    handler.setFormatter(logging.Formatter("%(asctime)s:%(levelname)s:%(name)s: %(message)s"))
    logger.addHandler(handler)
    handler_stream = logging.StreamHandler(sys.stdout)
    handler_stream.setFormatter(
        logging.Formatter("%(asctime)s:%(levelname)s:%(name)s: %(message)s"))
    logger.addHandler(handler_stream)
    return logger

running = True

logger = init_logger()
config = Config(logger)
config.load_conf()
config.load_welcome_info()
config.load_tokens()
sheriff = Bot(command_prefix=config.bot_prefix)
db = Database(config, logger)


async def close(exit_code=0):
    global running
    running = False
    db.close_db()
    await sheriff.close()
    # await asyncio.get_event_loop().stop()
    sys.exit(exit_code)


async def dialog_yn(cmd_name: str, question: str, author, channel, clb, admin_needed=False):
    """Discord dialog with yes/no answer."""
    if not admin_needed or config.role_admin in author.roles:
        await channel.send(f"{question} ? [y/n] - {author.mention}")
        logger.debug(f"{question} ? [y/n] - {author.mention}")

        def check(msg):
            return msg.author == author and msg.channel == channel

        msg = await sheriff.wait_for("message", check=check)
        if msg:
            if msg.content.lower() == "y" or msg.content.lower() == "yes":
                await channel.send(f"{cmd_name} in progress...")
                logger.debug(f"{cmd_name} in progress...")
                await asyncio.sleep(3)
                # calls callback
                await clb()
            elif msg.content.lower() == "n" or msg.content.lower() == "no":
                await channel.send(f"{cmd_name} cancelled.")
                logger.debug(f"{cmd_name} cancelled.")
            else:
                await channel.send("User didn't enter a valid option.")
                logger.debug("User didn't enter a valid option.")
    else:
        msg = f"You are not allowed to use {cmd_name} command! (Admin only command)"
        await channel.send(msg)
        logger.debug("{msg} - {author}")


async def check_restart():
    """Checks if the bot has been restarted or updated."""
    if len(sys.argv) == 3:
        if sys.argv[2] == "0":
            await config.bot_channel.send("Updated successfully :ballot_box_with_check:")
            logger.getChild("check_restart").info("Updated successfully.")
        else:
            await config.bot_channel.send("Update failed :poop:")
            logger.getChild("check_restart").warning("Update failed.")
    # if this is a restarted instance -> inform user about the restart
    elif len(sys.argv) == 2 and sys.argv[1] == "restart":
        await config.bot_channel.send("Restarted :ballot_box_with_check:")
        logger.getChild("check_restart").info("Restarted.")


def init_roles():
    """Initialize variables with server roles."""
    try:
        logger.getChild("on_ready").info(config.separator + "Roles:")
        for role in config.server.roles:
            if role.name == config.role_admin_name:
                config.role_admin = role
                logger.getChild("init_roles").info(f"Admin role: {config.role_admin_name}")
            elif role.name == config.role_newcomer_name:
                config.role_newcomer = role
                logger.getChild("init_roles").info(f"Newcomer role: {config.role_newcomer_name}")
            elif role.name == config.role_member_name:
                config.role_member = role
                logger.getChild("init_roles").info(f"Member role: {config.role_muted_name}")
            elif role.name == config.role_muted_name:
                config.role_muted = role
                logger.getChild("init_roles").info(f"Muted role: {config.role_muted_name}")
    except Exception as e:
        logger.getChild("init_roles").error(f"{e}")


async def check_users(midnight=False):
    """Check which users are online. If they are not yet logged for current day, add login record
     for them.Fix conflicting user roles."""
    try:
        members = sheriff.get_all_members()
        online_members = []
        for member in members:
            if member.bot:
                continue
            # in case the user does not exist, create him - duplicates managed by the DB
            db.add_new_user(member.id, member.name, datetime.datetime.utcnow())
            # in case of user with no role
            if len(member.roles) == 1 and member.roles[0].name == "@everyone":
                # if already muted user is trying to rejoin the sever, assign him/her the Muted role
                if db.is_muted(member.id):
                    await change_member_role(member, config.role_muted)
                # otherwise start the newcomer countdown
                else:
                    asyncio.ensure_future(promote_to_newcomer(member))

            # online users
            if member.status.value != "offline" and member.status.value != "invisible":
                online_members.append(member)
                db.insert_login_time(member.id, datetime.datetime.utcnow(), midnight)

            # fix conflicting roles - if a user is both Newcomer and Member -> remove Newcomer role.
            if config.role_admin in member.roles:
                continue
            if config.role_newcomer in member.roles and config.role_member in member.roles:
                await remove_member_roles(member, config.role_newcomer)
            if (config.role_newcomer in member.roles or config.role_member in member.roles) and \
                    config.role_muted in member.roles:
                await remove_member_roles(member, config.role_newcomer, config.role_member)

        return members, online_members
    except Exception as e:
        logger.getChild("check_users").error(f"{e}")
        return None, None


async def promote_to_newcomer(user: discord.Member):
    """Promote user to Newcomer after config.promote_to_newcomer_hours hours"""
    try:
        logger.getChild("promote_to_newcomer").info(f"newcomer countdown {user.name}")
        db.add_new_user(user.id, user.name, datetime.datetime.utcnow())
        await asyncio.sleep(config.promote_to_newcomer_hours * 60 * 60)
        if config.role_member not in user.roles and config.role_newcomer not in user.roles \
                and config.role_muted not in user.roles:
            await change_member_role(user, config.role_newcomer)
    except Exception as e:
        logger.getChild("promote_to_newcomer").error(f"{e}")


async def promote_to_member(user: discord.Member):
    try:
        members = sheriff.get_all_members()
        for member in members:
            if member.id == user.id:
                # muted user cannot be promoted to Member
                if config.role_muted not in member.roles:
                    await change_member_role(member, config.role_member)
                break
    except Exception as e:
        logger.getChild("promote_to_member").error(f"{e}")


async def change_member_role(member: discord.Member, role: discord.Role):
    """Changes the role for member to role with name role_name."""
    logger.getChild("change_member_role").info(f"Changing user role of {member.display_name} to "
                                               f"{role.name}.")
    if role == config.role_newcomer:
        await add_member_roles(member, config.role_newcomer)
    elif role == config.role_member:
        await add_member_roles(member, config.role_member)
        await remove_member_roles(member, config.role_newcomer)
    elif role == config.role_muted:
        await remove_member_roles(member, config.role_newcomer, config.role_member)
        await add_member_roles(member, config.role_muted)
    else:
        logger.warning(f"Can not change role for user {member.display_name} to: {role.name}")


async def add_member_roles(member, *roles):
    try:
        if config.role_muted in roles:
            db.set_muted(member.id, True)
        for role in roles:
            logger.info(f"Adding role {role.name} to: {member.display_name}")
            await member.add_roles(role)
    except Exception as e:
        logger.getChild("add_member_roles").error(f"{e}")


async def remove_member_roles(member, *roles):
    try:
        # logger.debug("Removing roles: {}".format(*roles.name))
        if config.role_muted in roles:
            db.set_muted(member.id, False)
        for role in roles:
            logger.info(f"Removing role {role.name} from: {member.display_name}")
            await member.remove_roles(role)
    except Exception as e:
        logger.getChild("remove_member_roles").error(f"{e}")


async def punish(member: discord.Member, channel: discord.TextChannel):
    # admin and bots can not be banned
    if config.role_admin in member.roles or member.bot:
        return
    try:
        if config.punish_action == "ban":
            await member.ban(delete_message_days=0)
        elif config.punish_action == "kick":
            await member.kick()
        elif config.punish_action == "mute":
            await change_member_role(member, config.role_muted)
        await channel.send(f"Punishing {member} with punish action: {config.punish_action}")
        logger.info(f"Punishing {member} with punish action: {config.punish_action}")
    except Exception as e:
        logger.getChild("punish").error(f"{e}")


# CHECK
async def restart_program(update_result=None):
    """Restarts the bot."""
    try:
        await asyncio.sleep(3)
        # in case of update restart
        if update_result is not None:
            # works only in the Python docker, otherwise /usr/bin/python3 should be used as execl file
            os.execl("/usr/local/bin/python3", "python3", "sheriff.py", "restart",
                     f"{update_result}")
        else:
            os.execl("/usr/local/bin/python3", "python3", "sheriff.py", "restart")
    except Exception as e:
        logger.getChild("restart_program").error(f"{e}")


# CHECK
async def update_program():
    try:
        result = os.system("git pull")
        logger.info(result)
        await restart_program(result)
    except Exception as e:
        logger.getChild("update_program").error(f"{e}")


# ~~~~~ loops ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
async def day_loop():
    """Runs every day at midnight."""
    tomorrow = datetime.datetime.utcnow() + datetime.timedelta(days=1)
    midnight = datetime.datetime(year=tomorrow.year, month=tomorrow.month, day=tomorrow.day,
                                 hour=0, minute=0, second=0)
    secs_next_day = (midnight - datetime.datetime.utcnow()).seconds
    while running:
        try:
            # sleep until next midnight
            await asyncio.sleep(secs_next_day + 60)  # 60 sec reserve in case of inaccuracy
            secs_next_day = 86400

            # prune users inactive for config.prune_days number of days
            db.prune_users()
            await config.server.prune_members(config.prune_days)

            await check_users(True)
        except Exception as e:
            logger.getChild("check_users").error(f"{e}")


# ~~~~~ events ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@sheriff.event
async def on_ready():
    """ Initialization, starting loops."""
    logger.getChild("on_ready").info("Logged in as")
    logger.getChild("on_ready").info(sheriff.user.name)
    logger.getChild("on_ready").info(sheriff.user.id)
    logger.getChild("on_ready").info(config.separator)

    # find the right discord server
    for server in sheriff.guilds:
        if server.id == config.server_id or server.name == config.server_name:
            config.server = server
            break

    # Roles
    init_roles()

    # Users
    logger.getChild("on_ready").info(config.separator + "Users:")
    members, online_members = await check_users()
    for user in online_members:
        role_names = []
        for role in user.roles:
            role_names.append(role.name)
        logger.getChild("on_ready").info(f"{user.name} ({user.status}) - {role_names}")

    config.bot_channel = sheriff.get_channel(config.bot_channel_id)

    # loops
    sheriff.loop.create_task(day_loop())
    # sheriff.loop.create_task(check_users_loop())

    # checks if the bot has been restarted or updated.
    await check_restart()


@sheriff.event
async def on_message(message):
    try:
        # makes the commands work
        if message.author.bot:
            return
        if len(message.content) > 0 and message.content[0] != config.bot_prefix:
            logger.debug(f"NEW MESSAGE: {message.content}")
            if db.increment_message_count(message.author.id):
                await promote_to_member(message.author)
        else:
            await sheriff.process_commands(message)
    except Exception as e:
        logger.getChild("on_message").error(f"{e}")


@sheriff.event
async def on_message_delete(message):
    try:
        logger.debug(f"DELETED: {message.content}")
        db.decrement_message_count(message.author.id)
    except Exception as e:
        logger.getChild("on_message_delete").error(f"{e}")


# CHECK
@sheriff.event
async def on_reaction_add(reaction, user):
    try:
        logger.getChild("on_reaction_add").debug(f"{user.name} added reaction: {reaction.emoji}")

        # check if this might be a punish case
        if reaction.emoji == config.punish_emoji and reaction.count >= config.punish_vote_count:
            member_votes = 0
            # check if there is enough Members punish votes
            users = await reaction.users().flatten()
            for user in users:
                member = config.server.get_member(user.id)
                if config.role_member in member.roles:
                    member_votes += 1
            if member_votes >= config.punish_vote_count:
                msg = reaction.message
                await punish(msg.author, msg.channel)
    except Exception as e:
        logger.getChild("on_reaction_add").error(f"{e}")


@sheriff.event
async def on_member_join(member):
    try:
        if len(member.roles) == 1 and member.roles[0].name == "@everyone":
            await member.send(config.welcome_info)
            if db.is_muted(member.id):
                await change_member_role(member, config.role_muted)
            else:
                await promote_to_newcomer(member)
    except Exception as e:
        logger.getChild("on_member_join").error(f"{e}")


@sheriff.event
async def on_member_update(before, after):
    try:
        # if user changed his/her status to online from offline
        if (before.status.value == "offline" or before.status.value == "invisible") and \
                after.status.value != "offline" and after.status.value != "invisible":
            if db.insert_login_time(after.id, datetime.datetime.utcnow()):
                await promote_to_member(after)
    except Exception as e:
        logger.getChild("on_member_update").error(f"{e}")


# ~~~~~ Commands ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# CHECK
@sheriff.command(name="shutdown",
                 description="Turns off the Sheriff bot. (Admin only command)",
                 brief="Turns off the Sheriff bot. (Admin only)",
                 pass_context=True)
async def shutdown(ctx):
    """Turns off the Sheriff bot. (Admin only command)"""
    try:
        author = ctx.message.author
        await dialog_yn("shutdown", "Are you sure you want to shutdown the Sheriff bot ?",
                  author, ctx.message.channel, close, admin_needed=True)
    except Exception as e:
        logger.getChild("!shutdown").error(f"{e}")


# CHECK
@sheriff.command(name="restart",
                 description="Restarts the Sheriff bot. (Admin only command)",
                 brief="Restarts the Sheriff bot. (Admin only)",
                 pass_context=True)
async def restart(ctx):
    """Restarts the Sheriff bot. (Admin only command)"""
    try:
        author = ctx.message.author
        await dialog_yn("restart", "Are you sure you want to restart the Sheriff bot ?", author,
                  ctx.message.channel, restart_program, admin_needed=True)
    except Exception as e:
        logger.getChild("!restart").error(f"{e}")


# CHECK
@sheriff.command(name="update",
                 description=("Updates bot from the repository and restarts the bot. "
                              "(Admin only command)"),
                 brief="Updates bot from the repository. (Admin only)",
                 pass_context=True)
async def update(ctx):
    """Updates the Sheriff bot the newest git version. (Admin only command)"""
    try:
        author = ctx.message.author
        await dialog_yn("update", "Are you sure you want to update and restart the Sheriff bot ?",
                  author, ctx.message.channel, update_program, admin_needed=True)
    except Exception as e:
        logger.getChild("!update").error(f"{e}")


# CHECK
@sheriff.command(name="stats",
                 description="Shows basic Discord server stats.",
                 brief="Shows basic Discord server stats.",
                 pass_context=True)
async def stats(ctx, user_name=""):
    """Shows information about its work - how many users have been promoted and how many have been
    punished."""
    try:
        # general stats
        if user_name is None or len(user_name) == 0:
            # query_msgs = "SELECT SUM(message_count) FROM users;"
            # data_msgs = db.execute_select_query_one(query_msgs)
            # query_muted = "SELECT COUNT(user_id) FROM users WHERE muted=1"
            # data_muted = db.execute_select_query_one(query_muted)
            data_msgs, data_muted = db.get_stats()

            members = sheriff.get_all_members()
            counter_mem = 0
            counter_new = 0
            for member in members:
                if config.role_member in member.roles:
                    counter_mem += 1
                elif config.role_newcomer in member.roles:
                    counter_new += 1

            result = "**Stats since 01.01.2019\n**"
            result += f"Number of Members: {counter_mem}\n"
            result += f"Number of Newcomers: {counter_new}\n"
            result += f"Number of Muted users: {data_muted}\n"
            result += f"Total number of messages: {data_msgs}\n"

            await config.bot_channel.send(result)
            logger.info(f"Stats:\n{result}")
        # user specific stats - msg count, days count, last login date
        else:
            if user_name == "me":
                user_name = ctx.message.author.mention
            for member in sheriff.get_all_members():
                if member.name == user_name or member.mention == user_name or \
                        member.display_name == user_name or str(member.id) == user_name:
                    # query_msgs = "SELECT message_count, days_count, last_login FROM users WHERE user_id={};".format(member.id)
                    # data = db.execute_select_query_one(query_msgs)
                    data = db.get_stats_user(member.id)
                    result = f"**Stats for {member.display_name}\n**"
                    # DELETE
                    result += f"Message count: {data.message_count}\n"  # .format(data[0])
                    result += f"Days online: {data.days_count}\n"  # .format(data[1])
                    last_login = datetime.datetime.strptime(
                        data.last_login, "%y-%m-%dT%H:%M:%S").strftime("%d.%m.%Y")
                    result += f"Last login: {last_login}"  # .format()
                    logger.info(f"Stats for {member.display_name}")  # .format(member.display_name))
                    await config.bot_channel.send(result)
                    return
            await config.bot_channel.send(f"Cannot find user {user_name}. Try to use mention "
                                          f"instead of nick.")
            logger.info(f"Cannot find user {user_name}. Try to use mention instead of nick.")
    except Exception as e:
        logger.getChild("!stats").error(f"{e}")


# TODO simplify
@sheriff.command(name="mute",
                 description="Sets the Muted flag for the user to True and mutes him/her.",
                 brief="Mutes user.",
                 pass_context=True)
async def mute(ctx, *args):
    try:
        if not args:
                logger.getChild("?mute").warning(f"No username !")
                await config.bot_channel.send(f"No username !")
                return
        user_name = args[0]
        if config.role_admin in ctx.message.author.roles:
            for member in sheriff.get_all_members():
                if member.name == user_name or member.mention == user_name or \
                        member.display_name == user_name or str(member.id) == user_name:
                    await change_member_role(member, config.role_muted)
                    await config.bot_channel.send(f"Muting {user_name}.")
                    logger.getChild("?mute").info(f"Muting {user_name}.")
                    return
            await config.bot_channel.send(
                f"Cannot find user {user_name}. Try to use mention instead of nick.")
            logger.getChild("?mute").info(f"Cannot find user {user_name}. Try to use mention "
                                          f"instead of nick.")
        else:
            await config.bot_channel.send(
                "You are not allowed to use mute command! (Admin only command)")
            logger.getChild("?mute").debug(
                f"You are not allowed to use mute command! (Admin only command) - "
                f"{ctx.message.author}")
    except Exception as e:
        logger.getChild("mute").error(f"{e}")


# TODO simplify
@sheriff.command(name="unmute",
                 description="Sets the Muted flag for the user to False and unmutes him/her.",
                 brief="Disables mute for the user.",
                 pass_context=True)
async def unmute(ctx, *args):
    try:
        if not args:
                logger.getChild("?unmute").warning(f"No username !")
                await config.bot_channel.send(f"No username !")
                return
        user_name = args[0]
        if config.role_admin in ctx.message.author.roles:

            for member in sheriff.get_all_members():
                if member.name == user_name or member.mention == user_name or \
                        member.display_name == user_name:
                    await remove_member_roles(member, config.role_muted)
                    await config.bot_channel.send(f"Unmuting {user_name}.")
                    logger.getChild("?unmute").info(f"Unmuting {user_name}.")
                    await promote_to_newcomer(member)
                    return
            await config.bot_channel.send(
                f"Cannot find user {user_name}. Try to use mention instead of nick.")
            logger.getChild("?unmute").info(
                f"Cannot find user {user_name}. Try to use mention instead of nick.")
        else:
            await config.bot_channel.send(
                "You are not allowed to use update command! (Admin only command)")
            logger.getChild("?unmute").debug(
                f"You are not allowed to use update command! (Admin only command) - "
                f"{ctx.message.author}")
    except Exception as e:
        logger.getChild("mute").error(f"{e}")


sheriff.run(config.token)
sheriff.close()
